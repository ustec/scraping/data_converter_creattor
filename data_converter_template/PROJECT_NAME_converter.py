from data_converter.data_converter import DataConverter
from config import Config{{PROJECT_NAME_CAMEL}}
from {{PROJECT_NAME}} import {{PROJECT_NAME_CAMEL}}
import logging
from sqlalchemy.orm import mapper

class {{PROJECT_NAME_CAMEL}}Converter(DataConverter):
            
    def __init__(self, config_file_path):
        self.config = Config{{PROJECT_NAME_CAMEL}}(config_file_path)
        super({{PROJECT_NAME_CAMEL}}Converter, self).__init__()
        mapper({{PROJECT_NAME_CAMEL}}, self.table) 
        logging.basicConfig(filename=self.config.log_file, level=logging.WARNING)
    
    def get_input_files(self):
        pass
        
    def convert(self):
        pass
         
    def get_valid_records(self):
        pass
        
    def build_objects(self, raw_records):
        pass