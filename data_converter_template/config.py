from data_converter.config import ConfigMySql

class Config{{PROJECT_NAME_CAMEL}}(ConfigMySql):

    def __init__(self, config_file_path):
        super(Config{{PROJECT_NAME_CAMEL}}, self).__init__(config_file_path)