from data_converter.output_object import OutputObject

class {{PROJECT_NAME_CAMEL}}(OutputObject):
    def __init__(self):
        self.field1 = ""
        self.field2 = ""
        self.field3 = 0
        
    def __str__(self):
        return "[field1: " + self.field1 + \
               ", field2: " + self.field2 + \
               "]"