#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# (c) 2019 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import re
import shutil

def snake2camel(name):
    return re.sub(r'(?:^|_)([a-z])', lambda x: x.group(1).upper(), name)

parser = argparse.ArgumentParser()
#parser.add_argument("-l", "--localdata", action='store_true')
parser.add_argument("project_name")
parser.add_argument("-b", "--project_base_path", default="/home/monica/sindicat/scraping")
parser.add_argument("-s", "--host", default="localhost")
parser.add_argument("-u", "--user", default="ustec")
parser.add_argument("-p", "--password", default="ustec")
parser.add_argument("-d", "--database_name", default="ustec")
parser.add_argument("-t", "--table_name")

args = parser.parse_args()

project_name = args.project_name.lower()
project_base_path = args.project_base_path
host = args.host
user = args.user
password = args.password
database_name = args.database_name
table_name = args.table_name

project_name_camel = snake2camel(project_name)
project_path = os.path.join(project_base_path, project_name)
if not table_name:
    table_name = project_name

# Create directory project
shutil.copytree("data_converter_template", project_path)

# Change file names with project_name
os.rename(os.path.join(project_path, "PROJECT_NAME_converter.py"), os.path.join(project_path, project_name + "_converter.py"))
os.rename(os.path.join(project_path, "PROJECT_NAME.py"), os.path.join(project_path, project_name + ".py"))

# Change vars in files
for f in os.listdir(project_path):
    if os.path.isfile(os.path.join(project_path, f)):
        with open(os.path.join(project_path, f), 'r') as file :
            data = file.read()
        data = data.replace('{{PROJECT_PATH}}', project_path) \
            .replace('{{PROJECT_NAME}}', project_name) \
            .replace('{{PROJECT_NAME_CAMEL}}', project_name_camel) \
            .replace('{{HOST}}', host) \
            .replace('{{USER}}', user) \
            .replace('{{PASSWORD}}', password) \
            .replace('{{DATABASE_NAME}}', database_name) \
            .replace('{{TABLE_NAME}}', table_name)
        with open(os.path.join(project_path, f), 'w') as file:
            file.write(data)